# ENTREGA CONVOCATORIA ENERO

**Nombre y Apellidos:** Gerardo Romero Mendoza

**Correo Universidad:** g.romerome@alumnos.urjc.es

**Enlace al video explicativo:** https://youtu.be/RiDcTeQPRcM

## PARTE BÁSICA (Funcionalidad Obligatoria)

### Signalling Server (signalling.py)

Servidor que escucha en un puerto UDP especifico que le permite recibir mensajes de señalización. Los tipos de mensajes que maneja incluyen:

   - Registro de streamers: Registra un archivo y la dirección del streamer.
   - Petición de lista de ficheros: Devuelve una lista de archivos registrados.
   - Oferta de navegador: Envía la oferta SDP del navegador al streamer.
   - Respuesta de streamer: Envía la respuesta SDP del streamer al navegador.
   
   - **Lanzamiento**: python3 signalling.py <signal_port>

### Streaming Server (streamer.py)

Servidor que se registra en el signalling y espera ofertas SDP de navegadores para iniciar una conexión WebRTC y transmitirles el video especificado.

   - **Lanzamiento**: python3 streamer.py <file> <signal_ip> <signal_port>
   
### Frontend Server (front.py)

Servidor web donde los usuarios pueden ver la lista de videos disponibles. Envía y recibe mensajes de señalización para establecer conexiones WebRTC entre los navegadores y los streamers.

   - **Lanzamiento**: python3 front.py <http_port> <signal_ip> <signal_port>

### Interfaz de Usuario (index.html)
Archivo con el diseño de la interfaz de usuario en el navegador.

### Errores:
   - El refresco de la página en el navegador hace que la funcionalidad del sistema desaparezca.
   - Stop no siempre funciona correctamente.

## PARTE ADICIONAL (Funcionalidad avanzada)

### Opciones realizadas

   - Streamers envían información adicional como el título de los videos, ocurre en el momento del registro con el signalling. (Visible en el Front)
   - Streamers envían información adicional como la descripción de los videos, ocurre en el momento del registro con el signalling. (Visible en el Front)






