import asyncio
import logging
import cv2
import json
import fractions
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaStreamTrack, MediaPlayer
from aiortc.mediastreams import VideoFrame


description = {"Vid1.mp4": "-Video1: Presenta una cuenta atrás animada, de estética infantil.",
               "Vid2.mp4": "-Video2: Muestra un paisaje urbano con contaminación. Se ven aviones sobre la ciudad.",
               "Vid3.mp4": "-Video3: Presenta un resumen de algunos de los sucesos más importantes de 2019."}
title = {"Vid1.mp4":"Video1",
         "Vid2.mp4":"Video2",
         "Vid3.mp4":"Video3"}

# Inicializar logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')


class VideoStreamTrack(MediaStreamTrack):
    kind = "video"

    def __init__(self, file, width=640, height=480):
        super().__init__()
        self.cap = cv2.VideoCapture(file)
        self.frame_time = 1 / self.cap.get(cv2.CAP_PROP_FPS)
        self.start_time = None
        self.width = width
        self.height = height
        if not self.cap.isOpened():
            logging.error(f"No se pudo abrir el archivo de video: {file}")

    async def recv(self):
        if self.start_time is None:
            self.start_time = asyncio.get_event_loop().time()
            self.pts = 0

        # Calcular el tiempo transcurrido y ajustar el frame
        pts, time_base = self.next_timestamp()

        ret, frame = self.cap.read()
        if not ret:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            ret, frame = self.cap.read()

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.resize(frame, (self.width, self.height))
        frame = VideoFrame.from_ndarray(frame, format="rgb24")

        # Incrementar pts por el tiempo de un frame
        self.pts += self.frame_time
        frame.pts = pts
        frame.time_base = time_base

        # Calcular el tiempo de dormir para mantener la tasa de cuadros por segundo
        elapsed = asyncio.get_event_loop().time() - self.start_time
        sleep_time = self.pts - elapsed
        if sleep_time > 0:
            await asyncio.sleep(sleep_time)

        return frame

    def next_timestamp(self):
        """
        Genera una nueva marca de tiempo para el frame.
        """
        frame_time = self.frame_time
        pts = int(self.cap.get(cv2.CAP_PROP_POS_FRAMES) * frame_time * 1000)
        time_base = fractions.Fraction(1, 1000)
        return pts, time_base

class StreamerProtocol:
    def __init__(self, pc, file, title, description):
        self.pc = pc
        self.file = file
        self.title = title
        self.description = description

    def connection_made(self, transport):
        self.transport = transport
        logging.info("Conexión establecida")

    def datagram_received(self, data, addr):
        message = data.decode()
        logging.info(f"Mensaje recibido de {addr}: {message}")

        try:
            message = json.loads(message)
        except json.JSONDecodeError:
            logging.error("Error decodificando el mensaje")
            return

        if message["type"] == "OFFER":
            offer = RTCSessionDescription(sdp=message["sdp"], type='offer')
            logging.info(f"Oferta SDP recibida: {offer.sdp}")
            asyncio.ensure_future(self.handle_offer(offer, addr))

    async def handle_offer(self, offer, addr):
        await self.pc.setRemoteDescription(offer)
        logging.info("Descripción remota configurada")

        media_player = MediaPlayer(self.file)
        # Verificar y agregar pistas de medios necesarias
        for transceiver in self.pc.getTransceivers():
            if transceiver.kind == "video" and transceiver.receiver.track is None:
                self.pc.addTrack(VideoStreamTrack(self.file))
                logging.info("Pista de video añadida al transceptor")
            elif transceiver.kind == "audio" and transceiver.receiver.track is None:
                self.pc.addTrack(media_player.audio)
                logging.info("Pista de audio añadida al transceptor")

        # Asegurarse de que las direcciones de los transceptores estén configuradas correctamente
        for transceiver in self.pc.getTransceivers():
            if transceiver.direction is None:
                transceiver.direction = "recvonly"
            if transceiver._offerDirection is None:
                transceiver._offerDirection = "recvonly"

            logging.info(f"Dirección actual: {transceiver.direction}, Oferta Dirección: {transceiver._offerDirection}")

        answer = await self.pc.createAnswer()
        logging.info("Respuesta SDP creada")

        await self.pc.setLocalDescription(answer)
        logging.info("Descripción local configurada")
        response = json.dumps(
            {"type": "ANSWER", "sdp": self.pc.localDescription.sdp, "file": self.file, "client_addr": addr}).encode()
        self.transport.sendto(response, addr)
        logging.info(f"Respuesta SDP enviada: {response}")


async def main(file, signal_ip, signal_port):
    logging.info("Iniciando servidor de streaming")

    pc = RTCPeerConnection()
    media_player = MediaPlayer(file)
    video_track = VideoStreamTrack(file)
    pc.addTrack(video_track)
    pc.addTrack(media_player.audio)
    logging.info("Pista de video añadida al RTCPeerConnection")

    loop = asyncio.get_event_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: StreamerProtocol(pc, file, title[file], description[file]),
        local_addr=('0.0.0.0', 0)
    )

    register_message = json.dumps(
        {"type": "REGISTER",
         "file": file,
         "title": title[file],
         "description": description[file]}).encode()
    transport.sendto(register_message, (signal_ip, signal_port))
    logging.info(f"Registro enviado a {signal_ip}:{signal_port}")

    try:
        await asyncio.sleep(3600)  # Mantener el servidor corriendo
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    import sys

    file = sys.argv[1]
    signal_ip = sys.argv[2]
    signal_port = int(sys.argv[3])
    asyncio.run(main(file, signal_ip, signal_port))
