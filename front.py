import asyncio
import logging
import json
from aiohttp import web

# Inicializar logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

class FrontProtocol:
    def __init__(self, loop, on_response):
        self.loop = loop
        self.on_response = on_response

    def connection_made(self, transport):
        self.transport = transport
        logging.info("Conexión establecida")

    def datagram_received(self, data, addr):
        message = data.decode()
        logging.info(f"Mensaje recibido de {addr}: {message}")
        self.on_response(message)

    def connection_lost(self, exc):
        if exc:
            logging.error(f"Error: {exc}")

    def error_received(self, exc):
        logging.error(f"Error recibido: {exc}")

async def request_directory(signal_ip, signal_port, loop):
    future = loop.create_future()

    def on_response(message):
        if not future.done():
            future.set_result(message)

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: FrontProtocol(loop, on_response),
        remote_addr=(signal_ip, signal_port)
    )

    message = "DIRECTORY_REQUEST".encode()
    transport.sendto(message, (signal_ip, signal_port))
    logging.info("Petición de directorio enviada")

    response = await future
    transport.close()
    logging.info("Recepción de directorio desde el servidor de señalización")
    return json.loads(response)

async def handle_offer(request):
    params = await request.json()
    sdp = params["sdp"]
    file = params["file"]

    future = loop.create_future()

    def on_response(message):
        if not future.done():
            future.set_result(message)

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: FrontProtocol(loop, on_response),
        remote_addr=(signal_ip, signal_port)
    )

    message = json.dumps({"type": "OFFER", "sdp": sdp, "file": file}).encode()
    transport.sendto(message, (signal_ip, signal_port))
    logging.info(f"Oferta SDP enviada al servidor de señalización: {sdp}")

    response = await future
    response_data = json.loads(response)
    transport.close()
    logging.info(f"Respuesta SDP recibida del servidor de señalización: {response_data['sdp']}")

    return web.json_response({"sdp": response_data["sdp"]})

async def index(request):
    return web.FileResponse('index.html')

async def get_videos(request):
    directory = await request_directory(signal_ip, signal_port, loop)
    return web.json_response(directory)

async def main(http_port, signal_ip, signal_port):
    global loop
    loop = asyncio.get_event_loop()

    app = web.Application()
    app.router.add_get('/', index)
    app.router.add_get('/videos', get_videos)
    app.router.add_post('/offer', handle_offer)

    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', http_port)
    logging.info(f"Servidor web escuchando en el puerto {http_port}")
    await site.start()

    try:
        await asyncio.sleep(3600)  # Mantener el servidor corriendo
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    import sys
    http_port = int(sys.argv[1])
    signal_ip = sys.argv[2]
    signal_port = int(sys.argv[3])
    asyncio.run(main(http_port, signal_ip, signal_port))
