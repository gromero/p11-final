import asyncio
import logging
import json

# Inicializar logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')

directory = {}
streamer_info = {}
pending_offers = {}

class SignalingServerProtocol:
    def connection_made(self, transport):
        self.transport = transport
        logging.info("Servidor de señalización iniciado")

    def datagram_received(self, data, addr):
        message = data.decode()
        logging.info(f"Mensaje recibido de {addr}: {message}")

        if message == "DIRECTORY_REQUEST":
            files = [
                {
                    "file": file,
                    "title": info["title"],
                    "description": info["description"]
                }
                for file, info in streamer_info.items()
            ]
            response = json.dumps(files).encode()
            self.transport.sendto(response, addr)
            logging.info(f"Petición de directorio de {addr}, enviando: {files}")
        else:
            try:
                message = json.loads(message)
            except json.JSONDecodeError:
                logging.error("Error decodificando el mensaje")
                return

            if message["type"] == "REGISTER":
                file = message["file"]
                title = message["title"]
                description = message["description"]
                directory[file] = addr
                streamer_info[file] = {
                    "title": title,
                    "description": description
                }
                logging.info(f"Registro de streamer para el archivo {file} desde {addr} con título '{title}' y descripción '{description}'")


            elif message["type"] == "OFFER":
                file = message["file"]
                sdp = message["sdp"]
                if file in directory:
                    streamer_addr = directory[file]
                    pending_offers[streamer_addr] = addr
                    offer_message = json.dumps({"type": "OFFER", "sdp": sdp, "client_addr": addr}).encode()
                    self.transport.sendto(offer_message, streamer_addr)
                    logging.info(f"Oferta SDP para {file} enviada a {streamer_addr}: {sdp}")
                else:
                    logging.warning(f"Archivo {file} no encontrado en el directorio")

            elif message["type"] == "ANSWER":
                sdp = message["sdp"]
                if addr in pending_offers:
                    client_addr = pending_offers.pop(addr)
                    answer_message = json.dumps({"type": "ANSWER", "sdp": sdp}).encode()
                    self.transport.sendto(answer_message, client_addr)
                    logging.info(f"Respuesta SDP enviada a {client_addr}: {sdp}")
                else:
                    logging.error(f"No se encontró la oferta pendiente para {addr}")

async def main(signal_port):
    logging.info("Comienzo del servidor de señalización")

    loop = asyncio.get_event_loop()
    listen = loop.create_datagram_endpoint(
        SignalingServerProtocol,
        local_addr=('0.0.0.0', signal_port)
    )
    transport, protocol = await listen

    try:
        await asyncio.sleep(3600)  # Mantener el servidor corriendo
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    import sys
    signal_port = int(sys.argv[1])
    asyncio.run(main(signal_port))
